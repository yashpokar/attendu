const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

module.exports = function ({ config, logger }) {
  return async function (req, res, next) {
    try {
      const header = req.headers['authorization'];

      if (! header) {
        return res.status(401).json({
          success: false,
          message: 'Authentication token is required.',
        });
      }

      const token = header.split(' ')[1];

      const { userId } = jwt.verify(token, config.secretKey);

      req.user = await mongoose.model('User').findById(userId);

      if (! req.user) {
        return res.statu(401).json({
          success: false,
          message: 'User does not exists.',
        });
      }

      next();
    } catch (error) {
      let message = 'Authentication failed.';

      switch (error.name) {
        case 'JsonWebTokenError':
          message = 'Invalid session token';
          break;

        case 'NotBeforeError':
          message = 'Session expired.';
          break;

        default:
          break;
      }

      return res.status(401).json({
        success: false,
        message,
      });
    }
  }
};
