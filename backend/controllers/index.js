module.exports = function ({ app, ...rest }) {
  app.use(require('./auth')(rest));
  app.use('/city', require('./cities')(rest));
};
