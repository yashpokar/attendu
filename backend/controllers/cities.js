const express = require('express');
const { body, validationResult } = require('express-validator');
const mongoose = require('mongoose');
const axios = require('axios');

const router = express.Router();

module.exports = function ({ logger, authMiddleware, config }) {
  router.get('/index', authMiddleware, async (req, res) => {
    let cities = await mongoose.model('City').find({}, { _id: 0, __v: 0 });

    if (! cities.length) {
      logger.info('Pulling cities.');

      try {
        const response = await axios('https://raw.githubusercontent.com/rahulattainu/cities/master/cities.json');

        logger.info('pulled all the cities successfully.');

        cities = response.data;

        await mongoose.model('City').insertMany(cities);
      } catch (error) {
        logger.error('could not pull cities from github', error);

        return res.status(500).json({
          success: false,
          message: 'Internal server error.',
        });
      }
    }

    return res.json({
      success: true,
      message: 'Fetched all cities successfully.',
      cities,
    });
  });

  return router;
};
