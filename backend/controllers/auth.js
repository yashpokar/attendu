const express = require('express');
const { body, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const router = express.Router();

module.exports = function ({ logger, config }) {
  router.post('/login', [
    body('username')
      .notEmpty()
      .withMessage('Username is required.')
      .trim()
      .bail()
      .escape(),

    body('password')
      .notEmpty()
      .withMessage('Password is required.'),
  ], async (req, res) => {
    logger.info('login request', req.params, req.body)

    const errors = validationResult(req);

    if (! errors.isEmpty()) {
      const errorMessages = {};

      errors.array().forEach(({ msg, param }) => {
        if (! (param in errorMessages)) {
          errorMessages[param] = [];
        }

        errorMessages[param].push(msg);
      });

      return res.status(422).json({
        success: false,
        message: 'Validation failed.',
        errors: errorMessages,
      });
    }

    const user = await mongoose.model('User').findOne({ username: req.body.username }, { _id: 1, password: 1 });

    if (! user || ! await bcrypt.compare(req.body.password, user.password)) {
      return res.status(401).json({
        success: false,
        message: 'Invalid email address or password, please try again.',
      });
    }

    const token = await jwt.sign({ userId: user._id }, config.secretKey, { expiresIn: '24h' });

    return res.json({
      success: true,
      message: 'You are logged-in successfully.',
      data: {
        user: {
          id: user._id,
          username: req.body.username,
        },
        token,
      }
    });
  });

  router.post('/register', [
    body('name')
      .notEmpty()
      .trim()
      .escape()
      .withMessage('Name is required.'),

    body('username')
      .notEmpty()
      .trim()
      .escape()
      .withMessage('Username is required.')
      .custom(async username => {
        const doesAlreadyExists = await mongoose.model('User').findOne({ username }, {});

        if (doesAlreadyExists) {
          throw new Error('Username already exists.');
        }
      }),

    body('password')
      .notEmpty()
      .withMessage('Password is required.')
      .bail()
      .isLength({ min: 6, max: 20 })
      .withMessage('Password should have atleast 6 characters & max. of 20 characters')
      .bail()
      .custom((password, { req }) => {
        if (password !== req.body.confirmPassword) {
          throw new Error('Password doesn\'t match.');
        }

        return true;
      }),
  ], async (req, res) => {
    logger.info('registration request', req.params, req.body);

    const errors = validationResult(req);

    if (! errors.isEmpty()) {
      const errorMessages = {};

      errors.array().forEach(({ msg, param }) => {
        if (! (param in errorMessages)) {
          errorMessages[param] = [];
        }

        errorMessages[param].push(msg);
      });

      return res.status(422).json({
        success: false,
        message: 'Validation failed.',
        errors: errorMessages,
      });
    }

    const { name, username, password } = req.body;

    let user = new mongoose.model('User')({
      name,
      username,
      password: await bcrypt.hash(password, 10),
    });

    user = await user.save();

    return res.status(201).json({
      success: true,
      message: 'Your account has been created successfully.',
      user: {
        id: user._id,
        name: user.name,
      },
    });
  });

  return router;
};
