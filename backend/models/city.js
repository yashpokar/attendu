const mongoose = require('mongoose');

const CitySchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true,
  },
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  state: {
    type: String,
    required: true,
  },
}, { timestamp: true });

module.exports = mongoose.model('City', CitySchema);
