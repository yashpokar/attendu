const express = require('express');
const mongoose = require('mongoose');
const winston = require('winston');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');
const path = require('path');
const cors = require('cors');
const config = require('./config');

const inProduction = process.env.NODE_ENV === 'production';

const app = express();

app.use(require('helmet')());
app.use(express.json());
app.use(cors({
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200,
}));

const accessLogStream = rfs.createStream('access.log', {
  interval: '1d',
  path: path.join(__dirname, 'storage', 'logs')
});

app.use(morgan('combined', { stream: accessLogStream }));

const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: path.resolve(__dirname, 'storage', 'logs', 'error.log'), level: 'error' }),
    new winston.transports.File({ filename: path.resolve(__dirname, 'storage', 'logs', 'combined.log') }),
  ],
});

if (! inProduction) {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

mongoose.connect(config.mongodbUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

mongoose.connection.on('error', () => {
  logger.error('Database connection failed.');
});

mongoose.connection.on('open', () => {
  logger.debug('Database connected successfully.');
});

const authMiddleware = require('./authMiddleware')({ config, logger });

require('./models')();
require('./controllers')({ app, logger, config, authMiddleware });

app.listen(config.port, () => console.log(`server started at port ${config.port}`));
