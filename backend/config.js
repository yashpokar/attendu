const path = require('path');
const mode = process.env.NODE_ENV;

// TODO :: (yash) :: throw exception if dot env not exists for perticular environment
require('dotenv').config({ path: path.resolve(__dirname, `.env.${mode}`) });

module.exports = {
	port: process.env.PORT,
  mongodbUri: process.env.MONGODB_URI,
  secretKey: process.env.SECRET_KEY,
};
