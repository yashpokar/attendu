import React, { useState } from 'react';
import swal from 'sweetalert';

import { useApp } from './../context/AppContext';

function Login({ history }) {
  const [data, setData] = useState({ username: '', password: '' });

  const onChange = (field, value) => {
    setData(prevData => {
      return {
        ...prevData,
        [field]: value,
      };
    });
  };

  const { login } = useApp();

  const loginUser = e => {
    e.preventDefault();

    login(data)
      .then(data => {
        history.push('/');
      })
      .catch(error => {
        if (! error.response || ! error.response.data || ! error.response.status) {
          return;
        }

        switch (error.response.status) {
          case 401:
            swal({
              title: 'Whoops!',
              text: error.response.data.message,
              icon: 'error',
            });
            break;

          case 422:
            // TODO :: handle validation error
            break;

          default:
            break;
        }
      });
  };

  return (
    <div className="flex items-center justify-center min-h-screen">
      <div className="shadow bg-white rounded w-4/12">
        <h3 className="font-semibold px-4 py-3 text-center tracking-wide text-xl">LOGIN</h3>

        <hr />

        <form onSubmit={loginUser} className="px-6">
          <div className="flex flex-col py-2">
            <label htmlFor="username" className="font-medium text-gray-600 cursor-pointer">Username</label>

            <div className="mt-1">
              <input
                type="text"
                id="username"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('username', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="flex flex-col py-2">
            <label htmlFor="password" className="font-medium text-gray-600 cursor-pointer">Password</label>

            <div className="mt-1">
              <input
                type="password"
                id="password"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('password', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="mt-2 mb-6">
            <button className="w-full text-sm bg-indigo-600 focus:outline-none focus:bg-indigo-800 hover:bg-indigo-800 transition ease-in-out duration-200 text-white font-medium tracking-wide rounded py-3">
              SUBMIT
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
