import React, { useState } from 'react';
import swal from 'sweetalert';

import { useApp } from './../context/AppContext';

function Register({ history }) {
  const [data, setData] = useState({ name: '', username: '', password: '' });

  const onChange = (field, value) => {
    setData(prevData => {
      return {
        ...prevData,
        [field]: value,
      };
    });
  };

  const { register } = useApp();

  const registerUser = e => {
    e.preventDefault();

    register(data)
      .then(({ message }) => {
        swal({
          title: 'Wel Done!',
          text: message,
          icon: 'success',
        });

        history.push('/login');
      });
  };

  return (
    <div className="flex items-center justify-center min-h-screen">
      <div className="shadow bg-white rounded w-4/12">
        <h3 className="font-semibold px-4 py-3 text-center tracking-wide text-xl">REGISTER</h3>

        <hr />

        <form onSubmit={registerUser} className="px-6">
          <div className="flex flex-col py-2">
            <label htmlFor="name" className="font-medium text-gray-600 cursor-pointer">Full name</label>

            <div className="mt-1">
              <input
                type="text"
                id="name"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('name', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="flex flex-col py-2">
            <label htmlFor="username" className="font-medium text-gray-600 cursor-pointer">Username</label>

            <div className="mt-1">
              <input
                type="text"
                id="username"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('username', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="flex flex-col py-2">
            <label htmlFor="password" className="font-medium text-gray-600 cursor-pointer">Password</label>

            <div className="mt-1">
              <input
                type="password"
                id="password"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('password', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="flex flex-col py-2">
            <label htmlFor="confirmPassword" className="font-medium text-gray-600 cursor-pointer">Password</label>

            <div className="mt-1">
              <input
                type="password"
                id="confirmPassword"
                className="w-full px-3 py-2 border-2 border-gray-200 focus:border-gray-400 transition ease-in-out duration-200 focus:outline-none rounded"
                onChange={e => onChange('confirmPassword', e.target.value)}
                required
              />
            </div>
          </div>

          <div className="mt-2 mb-6">
            <button className="w-full text-sm bg-indigo-600 focus:outline-none focus:bg-indigo-800 hover:bg-indigo-800 transition ease-in-out duration-200 text-white font-medium tracking-wide rounded py-3">
              CREATE ACCOUNT
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Register;
