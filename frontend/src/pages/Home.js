import React, { useEffect } from 'react';
import { useApp } from './../context/AppContext';

function Home() {
  const { fetchCities, cities } = useApp();

  useEffect(() => {
    if (! cities.length) {
      fetchCities();
    }
  }, [cities, fetchCities]);

  return (
    <div className="flex items-center justify-center py-4 min-h-screen">
      {cities.length ? (
        <ul className="flex flex-col">
          {cities.map(({ id, name, state }) => (
            <li className="flex justify-between items-center mb-4 bg-white shadow rounded p-4" key={id}>
              <span className="">{name}</span>
              <span className="text-sm text-gray-500 ml-2">{state}</span>
            </li>
          ))}
        </ul>
      ) : null}
    </div>
  );
}

export default Home;
