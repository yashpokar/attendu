import { createContext, useContext, useMemo, useReducer, useEffect } from 'react';
import axios from 'axios';

const AppContext = createContext(null);

function AppProvider({ children }) {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'LOGIN':
        return {
          ...state,
          isLoading: false,
          authToken: action.token,
          isAuthenticated: true,
        };

      case 'RESTORE_TOKEN':
        return {
          ...state,
          isLoading: false,
          authToken: action.token,
          isAuthenticated: true,
        };

      case 'LOGOUT':
        return {
          ...state,
          isLoading: false,
          authToken: null,
          isAuthenticated: false,
        };

      case 'FETCHED_CITIES':
        return {
          ...state,
          cities: action.cities,
        };

      default:
        return state;
    }
  }, {
    isLoading: true,
    authToken: null,
    isAuthenticated: false,
    cities: [],
  });

  const actions = useMemo(() => ({
    login: data => {
      return axios.post('/login', data)
      .then(({ data: { data } }) => {
        dispatch({ type: 'LOGIN', token: data.token });
        localStorage.setItem('token', data.token);

        return data;
      });
    },
    logout: async () => {
      localStorage.removeToken('token');
    },
    register: async data => {
      dispatch({ type: 'REQUEST_SERVER' });

      return axios.post('/register', data)
        .then(({ data }) => {
          dispatch({ type: 'RESPONSE_RECIEVED' });

          return data;
        });
    },
    fetchCities: () => {
      axios.get('/city/index')
        .then(({ data }) => {
          dispatch({ type: 'FETCHED_CITIES', cities: data.cities });
        });
    },
  }), []);

  useEffect(() => {
    if (! state.isAuthenticated && localStorage.token) {
      dispatch({ type: 'RESTORE_TOKEN', token: localStorage.token });
    }
  }, [state.isAuthenticated]);

  useEffect(() => {
    if (state.authToken) {
      axios.defaults.headers.common['Authorization'] = `Bearer ${state.authToken}`;
    } else {
      delete axios.defaults.headers.common['Authorization'];
    }
  }, [state.authToken]);

  return (
    <AppContext.Provider value={{ ...state, ...actions }}>
      {children}
    </AppContext.Provider>
  );
}

function useApp() {
  const context = useContext(AppContext);

  if (context === undefined) {
    throw new Error('useApp isnt available out of AppProvider');
  }

  return context;
}

export { useApp, AppProvider };
