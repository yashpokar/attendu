import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useApp } from './../context/AppContext';

export default function PrivateRoute({ component, ...rest }) {
  const Component = component;
  const { isAuthenticated, isLoading } = useApp();

  return (
    <Route
      {...rest}
      render={props =>
        ! isAuthenticated && ! isLoading ?
        <Redirect to={{ pathname: '/login', state: { from: props.location } }} /> :
        <Component {...props} />
      }
    />
  );
}
