import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { AppProvider } from './context/AppContext';
import PrivateRoute from './components/PrivateRoute';

const Home = lazy(() => import('./pages/Home'));
const Login = lazy(() => import('./pages/Login'));
const Register = lazy(() => import('./pages/Register'));

function App() {
  return (
    <AppProvider>
      <Router>
        <div className="w-full antialiased font-sans font-normal text-gray-700 bg-gray-100 min-h-screen">
          <Switch>
            <Suspense fallback={<div className="flex items-center h-screen justify-center">Loading...</div>}>
              <PrivateRoute
                path={'/'}
                component={Home}
                exact
              />

              <Route
                path={'/login'}
                component={Login}
                exact
              />

              <Route
                path={'/register'}
                component={Register}
                exact
              />
            </Suspense>
          </Switch>
        </div>
      </Router>
    </AppProvider>
  );
}

export default App;
